#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "drawwindow.h"
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_leString_textChanged(const QString &arg1);

    void on_leString1_textChanged(const QString &arg1);

    void on_pbConvert_clicked();

private:
    Ui::MainWindow *ui;
    DrawWindow *m_pDrawWindow;
};

#endif // MAINWINDOW_H
