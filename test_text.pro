#-------------------------------------------------
#
# Project created by QtCreator 2015-04-09T20:33:37
#
#-------------------------------------------------

QT       +=  gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = test_text
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    drawwindow.cpp \
    textdrawer.cpp \
    levenstain.cpp

HEADERS  += mainwindow.h \
    drawwindow.h \
    textdrawer.h \
    levenstain.h

FORMS    += mainwindow.ui \
    drawwindow.ui
