#include "drawwindow.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_pDrawWindow = new DrawWindow(this);
     m_pDrawWindow->setModal(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_leString_textChanged(const QString &text)
{
    if(text != "" && ui->leString1->text() != "")
        ui->pbConvert->setEnabled(true);
    else
        ui->pbConvert->setEnabled(false);


}

void MainWindow::on_leString1_textChanged(const QString &text)
{

    if(text != "" && ui->leString->text() != "")
        ui->pbConvert->setEnabled(true);
    else
        ui->pbConvert->setEnabled(false);

}

void MainWindow::on_pbConvert_clicked()
{

    m_pDrawWindow->Work(ui->leString->text(), ui->leString1->text());
}
