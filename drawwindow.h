#ifndef DRAWWINDOW_H
#define DRAWWINDOW_H
#include "levenstain.h"
#include "textdrawer.h"
#include <QDialog>

namespace Ui {
class DrawWindow;
}

class DrawWindow : public QDialog
{
    Q_OBJECT

public:
    explicit DrawWindow(QWidget *parent = 0);
    ~DrawWindow();
    void Work(QString s, QString s1);

private:
    Ui::DrawWindow *ui;
    TextDrawer *m_pDrawer;
    Levenstain *m_pLevenstain;
};

#endif // DRAWWINDOW_H
