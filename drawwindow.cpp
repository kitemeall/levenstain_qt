#include "drawwindow.h"
#include "ui_drawwindow.h"

DrawWindow::DrawWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DrawWindow)
{
    ui->setupUi(this);
    m_pDrawer = new TextDrawer(ui->gvDrawArea);
}
void DrawWindow::Work(QString s, QString s1)
{
    m_pDrawer->Clear();
    m_pLevenstain = new Levenstain(m_pDrawer,s,s1 );
    delete m_pLevenstain;
    show();
}

DrawWindow::~DrawWindow()
{
    delete m_pDrawer;
    delete ui;
}
