#include "levenstain.h"
#include <vector>
#include <stack>
Levenstain::Levenstain(TextDrawer * writer, QString s, QString s1)
{

    std::vector <std::vector <int> > mas (s.length()+1,
                                std::vector <int> (s1.length()+1, 0));
   std:: stack < char> likeness;

    for(int i = 0; i <=(int) s.length(); i++)
        mas[i][0] = i;
    for(int i = 0; i <= (int)s1.length(); i++)
        mas[0][i] = i;

    for(int i = 1; i <= (int)s.length(); i++)
    {
        for(int j = 1; j <= (int)s1.length(); j++ )
        {
            if(s[i - 1] == s1[j - 1])
            {
                mas[i][j] = mas[i - 1][j - 1];
            }
            else
            {
                mas [i][j] = std::min(std::min(mas[i - 1][j], mas[i][j - 1]),
                        mas[i-1][j-1]) + 1;


            }
        }
    }

    int i = s.length();
    int j = s1.length();
    int ins, rep, del;
    while(i != 0 || j != 0 )
    {

            if(i >= 1 && j >=1&&s[i - 1] == s1[j - 1])
            {
                likeness.push('M');
                --i; --j;
            }
            else
            {   if(j >= 1)
                     ins = mas[i][j - 1];
                if(i >= 1 && j >=1)
                     rep = mas[i - 1][j - 1];
                if(i >= 1)
                     del = mas[i - 1][j];

                if(j >= 1 && mas[i][j] == ins + 1)
                {
                    likeness.push('I');
                    --j;
                }
                else if (i >= 0 && mas[i][j] == del + 1)
                {
                    likeness.push('D');
                    --i;
                }
                else if (i >= 1 && j >=1 && mas[i][j] == rep + 1)
                {
                    likeness.push('R');
                    --i; --j;
                }
            }
    }
    std::stack <char> st1 = likeness;
    //operations count

   // cout << mas[s.length()][s1.length()] << endl;

    //operations count
    writer->WriteString("Преобразование строки ");
    writer->WriteString(s);
    writer->WriteString(" в строку");
    writer->WriteString(s1);
    int opCount = mas[s.length()][s1.length()];
    writer->NewLine();
    writer->WriteString("Количество операций: ");
    writer->WriteString(QString::number(opCount));
    writer->NewLine();





    j = 0;


    //cout << s << " --> ";
    writer->WriteString(s,true);

    while(!st1.empty())
    {
        if(st1.top() == 'I')
        {

            s.insert(j,s1[j]);
            writer->Inserted(s,j,true);
            j++;
           // cout << s << " --> ";
        }

        else if(st1.top() == 'R')
        {
            QString ts;
            ts.append(s[j]);
            s[j] = s1[j];
            writer->Replaced(s,j,ts, true);
            j++;
           // cout << s << " --> ";
        }
        else if(st1.top() == 'D')
        {
            QString ts;
            ts.append(s[j]);
            s.replace(j,1,"");
            writer->Deleted(s,j,ts,true);
            //cout << s << " --> ";
        }
        else if(st1.top() == 'M')
        {
            j++;
        }

        st1.pop();

    }
    writer->WriteString(s1);

}





Levenstain::~Levenstain()
{

}

