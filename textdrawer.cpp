#include "textdrawer.h"
#include <QGraphicsTextItem>
#include <QFont>
const QString TextDrawer::FONTFAMILY = "Courier New";
const int TextDrawer::STARTX = 10;
const int TextDrawer::STARTY = 30;
const int TextDrawer::TEXTPIXELSIZE = 20;
const QBrush TextDrawer::GREENBRUSH (QColor(20, 97, 30, 255));//green color
const QBrush TextDrawer::REDBRUSH (QColor(255, 0, 0, 255));//red color

const int TextDrawer::XINC = 20;
const int TextDrawer::YINC = 50;



TextDrawer::~TextDrawer()
{
    delete m_pFontMetrics;
    delete m_pCurrentText;
}

TextDrawer::TextDrawer(QGraphicsView *view):
    m_font(FONTFAMILY)

{
    m_pGraphiscViev = view;
    m_pCanvas = new QGraphicsScene(view);
    m_pGraphiscViev->setScene(m_pCanvas);
    m_pCanvas->setSceneRect(0, 0, view->width(),view->height()*3);
    m_width = view->width();
    m_height = view->height();

    m_font.setPixelSize(TEXTPIXELSIZE);
    m_pFontMetrics = new QFontMetrics(m_font);


    qDebug() << "TextDrawer created! Width = "<<m_width
             << "Height = "<< m_height;

    m_currentX = STARTX;
    m_currentY = STARTY;
    m_pCurrentText = 0;
    //tests




    // tests
}

void TextDrawer::Clear()
{
    m_pCanvas->clear();
    m_currentX = STARTX;
    m_currentY = STARTY;
    m_pCurrentText = 0;
}

void TextDrawer::WriteString(QString s, bool arrow)
{

    m_pCurrentText = m_pCanvas->addSimpleText(s,m_font);

    int textWidth = m_pFontMetrics->width(s);
    if(textWidth + m_currentX >= m_width)
    {
        m_currentX = STARTX;
        m_currentY += YINC;
    }
    m_pCurrentText->setPos(m_currentX, m_currentY);
    m_pCurrentText->setBrush(GREENBRUSH);

    m_currentX += textWidth;
    m_currentX += XINC;

    m_pCurrentText = 0;

    if(arrow)
    {
        WriteString("-->", false);
    }
}

void TextDrawer::NewLine()
{
    m_currentX = STARTX;
    m_currentY += YINC;
}

void TextDrawer::Replaced(QString s,int pos, QString oldChar,
                          bool arrow )
{
    int textWidth = m_pFontMetrics->width(s);
    if(textWidth + m_currentX >= m_width)
    {
        m_currentX = STARTX;
        m_currentY += YINC;
    }
    QString str1;
    QString redChar;
    QString str2;

    for( int i = 0; i< pos; i++)
        str1.append(s[i]);
    redChar.append(s[pos]);
    for(int i = pos + 1; i < s.length(); i++)
        str2.append(s[i]);


    m_pCurrentText = m_pCanvas->addSimpleText(str1,m_font);
    m_pCurrentText->setPos(m_currentX, m_currentY);
    m_pCurrentText->setBrush(GREENBRUSH);

    int str1Width = m_pFontMetrics->width(str1);
    m_currentX += str1Width;

    m_pCurrentText = m_pCanvas->addSimpleText(redChar,m_font);
    m_pCurrentText->setPos(m_currentX, m_currentY);
    m_pCurrentText->setBrush(REDBRUSH);

    m_pCurrentText = m_pCanvas->addSimpleText(oldChar,m_font);
    m_pCurrentText->setPos(m_currentX,
                           m_currentY - m_pFontMetrics->height() );
    m_pCurrentText->setBrush(GREENBRUSH);

    int redCharWidth = m_pFontMetrics->width(redChar);
    m_currentX += redCharWidth;

    m_pCurrentText = m_pCanvas->addSimpleText(str2,m_font);
    m_pCurrentText->setPos(m_currentX, m_currentY);
    m_pCurrentText->setBrush(GREENBRUSH);

    int str2Width = m_pFontMetrics->width(str2);
    m_currentX += str2Width;

    m_currentX += XINC;
    m_pCurrentText = 0;

    if(arrow)
    {
        WriteString("-->", false);
    }


}

void TextDrawer::Deleted (QString s,int pos, QString oldChar,
                          bool arrow )
{
    QString str;
    for( int i = 0; i < pos; i++)
        str.append(s[i]);
    str.append("_");

    for(int i = pos ; i < s.length(); i++)
        str.append(s[i]);
    Replaced(str, pos, oldChar, arrow);
}

void TextDrawer::Inserted(QString s,int pos,bool arrow )
{
    int textWidth = m_pFontMetrics->width(s);
    if(textWidth + m_currentX >= m_width)
    {
        m_currentX = STARTX;
        m_currentY += YINC;
    }
    QString str1;
    QString redChar;
    QString str2;

    for( int i = 0; i< pos; i++)
        str1.append(s[i]);
    redChar.append(s[pos]);
    for(int i = pos + 1; i < s.length(); i++)
        str2.append(s[i]);


    m_pCurrentText = m_pCanvas->addSimpleText(str1,m_font);
    m_pCurrentText->setPos(m_currentX, m_currentY);
    m_pCurrentText->setBrush(GREENBRUSH);

    int str1Width = m_pFontMetrics->width(str1);
    m_currentX += str1Width;

    m_pCurrentText = m_pCanvas->addSimpleText(redChar,m_font);
    m_pCurrentText->setPos(m_currentX, m_currentY);
    m_pCurrentText->setBrush(REDBRUSH);

    int redCharWidth = m_pFontMetrics->width(redChar);
    m_currentX += redCharWidth;

    m_pCurrentText = m_pCanvas->addSimpleText(str2,m_font);
    m_pCurrentText->setPos(m_currentX, m_currentY);
    m_pCurrentText->setBrush(GREENBRUSH);

    int str2Width = m_pFontMetrics->width(str2);
    m_currentX += str2Width;

    m_currentX += XINC;
    m_pCurrentText = 0;

    if(arrow)
    {
        WriteString("-->", false);
    }
}
