#ifndef LEVENSTAIN_H
#define LEVENSTAIN_H
#include "textdrawer.h"

class Levenstain
{
public:
    Levenstain(TextDrawer * writer, QString s, QString s1);
    ~Levenstain();
private:
    Levenstain();
};

#endif // LEVENSTAIN_H
