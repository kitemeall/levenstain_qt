#ifndef TEXTDRAWER_H
#define TEXTDRAWER_H
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QDebug>
#include <string>
class TextDrawer
{

public:
    TextDrawer(QGraphicsView *view);
    void WriteString(QString s,  bool arrow = false);
    void NewLine();
    void Replaced(QString s,int pos, QString oldChar,bool arrow = false );
    void Deleted(QString s,int pos, QString oldChar,bool arrow = false );
    void Inserted(QString s,int pos,bool arrow = false );
    void Clear();
    ~TextDrawer();
private:
    static const int STARTX;
    static const int STARTY;
    static const QString FONTFAMILY;
    static const int TEXTPIXELSIZE;
    static const QBrush GREENBRUSH;
    static const QBrush REDBRUSH;
    static const int XINC;
    static const int YINC;


    TextDrawer();
    QGraphicsView *m_pGraphiscViev;
    QGraphicsScene *m_pCanvas;

    int m_width;
    int m_height;

    int m_currentX;
    int m_currentY;\

    QFont m_font;
    QFontMetrics *m_pFontMetrics;
    QGraphicsSimpleTextItem *m_pCurrentText;

};

#endif // TEXTDRAWER_H
